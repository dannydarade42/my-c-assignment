
#include <stdio.h>
#include <string.h>

int main(int argc, char *argv[]){
    
    if(argc==1)
        printf("Enter 1 input string in command line");
        
    if(argc > 2)
        printf("Enter only 1 string.");
        
    char* str = argv[1];
    char* token = strtok(str, ",");

    while(token!=0){
        printf("%s\n", token);
        token = strtok(0, ",");
        
    }
 
    return 0;
}