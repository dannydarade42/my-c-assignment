#include <string.h>
#include <stdio.h>
void mergeSort();
void merge();


typedef struct
{
    int id;
    char name[10];
    int price;
} book;

char cmpname(const void *a, const void *b)
{
    const book *p1 = a;
    const book *p2 = b;
    return p1->name - p2->name;
}

int main(void)
{
    book b1[10] = {
        {1, "Book a", 100},
        {2, "Book e", 88},
        {3, "Book c", 110},
        {4, "Book b", 10},
        {5, "Book h", 1},
        {6, "Book d", 10},
        {7, "Book g", 90},
        {8, "Book j", 200},
        {9, "Book i", 20},
        {10, "Book k", 65}};

    int i;

     printf("\n------------Names in ascending order using quick sort------------------\n");

    qsort(b1, 10, sizeof(book), cmpname);

    for (i = 0; i < 10; i++)
    {
        printf(" \nId: %d \tName: %s \tPrice: %d", b1[i].id, b1[i].name, b1[i].price);
    }

    mergeSort(b1,0,9);
	printf("\n-------------------Price in descending oder using merge sort:-----------------\n\n");
	for(i=0;i<10;i++)
	{
		printf("ID: %d \t Name:  %s \t Price: %d\n",b1[i].id,b1[i].name,b1[i].price);

	}


    return 0;
}


//-----------------------Merge Sort logic-----------------------//

void mergeSort(book b1[],int first,  int last)
{
	int mid;
	if(first < last)
	{
		mid=(first+last)/2;
		mergeSort(b1,first,mid);
		mergeSort(b1,mid+1,last);
		merge(b1,first,last,mid);
	}
}

void merge(book b1[],int first,int last,int mid)
{
	book sortedArr[10];
	int i,j,k;
	i=first;
	j=mid+1;
	k=first;

	while(i<=mid && j<=last)
	{
		if(b1[i].price > b1[j].price)
			sortedArr[k++]=b1[i++];
		else
			sortedArr[k++]=b1[j++];
	}
	for(;i<=mid;i++)
		sortedArr[k++]=b1[i];
	for(;j<=last;j++)
		sortedArr[k++]=b1[j];
	for(i=first;i<=last;i++)
		b1[i]=sortedArr[i];
}


