#include <stdio.h>
#include <stdlib.h>
#include <string.h>									

typedef struct Item
{
	int id;
	char name[30];
	double price;
	int qty;
}item_t;											

enum Menu {ADD = 1, EDIT, DELETE, FIND, DISPLAY};

void add();
void edit();
void del();
void find();
void display();

int main()												
{
	enum Menu choice;
	do
	{
		printf("\n0. Exit"
			   "\n1. Add Item"
			   "\n2. Edit Item"
			   "\n3. Delete Item"
			   "\n4. Find Item"
			   "\n5. Display All Items"
			   "\n6. Exit"
			   "\nEnter your choice : ");
		scanf("%d", &choice);
		switch(choice)
		{
			case ADD:
				add();
				printf("\nItem added successfully...");
				break;
			case EDIT:
				edit();
				printf("\nItem edited successfully...");
				break;
			case DELETE:
				del();
				printf("\nItem deleted successfully...");
				break;
			case FIND:
				find();
				break;
			case DISPLAY:
				display();
				break;
		}
	}while(choice != 0);
	return 0;
}

void add()
{
	item_t i;
	i.id = 0;
	long int size = sizeof(item_t);
	FILE *fptr;
	fptr = fopen("inventory.db","ab+");
	if(fptr == NULL)
	{
		printf("\nFile can not be opened");
		return;
	}
	fseek(fptr,  -size, SEEK_END);
	fread(&i, sizeof(item_t), 1, fptr);
	i.id++;
	printf("\nEnter name of item : ");
	scanf("%s", i.name);
	printf("\nEnter price of item : ");
	scanf("%lf", &i.price);
	printf("\nEnter quantity of item : ");
	scanf("%d", &i.qty);	
	fseek(fptr, size, SEEK_END);
	fwrite(&i, size, 1, fptr);
	fclose(fptr);	
	return;
}

void edit()
{
	item_t i;
	FILE *fptr;
	long int size = sizeof(item_t);
	int temp, choice;
	printf("Enter the ID : ");
	scanf("%d", &temp);
	fptr = fopen("inventory.db", "rb+");
	if(fptr == NULL)
	{
		printf("\nFile can not be opened");
		return;
	}
	while(fread(&i, size, 1, fptr) > 0)
	{
			if(temp == i.id)
		{
			break;	
		}
	}
	printf("\nID : %d", i.id);
	printf("\nName : %s", i.name);
	printf("\nPrice : %.2lf", i.price);
	printf("\nQuantity : %d", i.qty);
	printf("\n\nSelect the data to be modified : ");
	printf("\n1. Name of item"
		   "\n2. Price of item"
		   "\n3. Quantity of item\n");
	scanf("%d", &choice);
	switch(choice)
	{
		case 1 :
			printf("Enter the name to be modified : ");
			scanf("%s", i.name);
			break;
		case 2 :
			printf("Enter the modified price of item : ");
			scanf("%lf", &i.price);
			break;
		case 3 :
			printf("Enter the modified quantity : ");
			scanf("%d", &i.qty);
			break;
	}
		fseek(fptr, -size, SEEK_CUR);
		fwrite(&i, size, 1, fptr);
		fclose(fptr);
	return ;
}

void del()
{
	item_t i;
	int j = 0;
	long int size = sizeof(item_t);
	int temp, found = 0;
	FILE *fptr, *fptr1;
	display();
	printf("\nEnter ID of item to be deleted from following : ");
	scanf("%d", &temp);											//enter id of data to be deleted
	fptr = fopen("inventory.db", "rb");							//original file in which data is stored
	if(fptr == NULL)
	{
		printf("\nFailed to open file....");
		return;
	}
	fptr1 = fopen("temp.db", "wb");								//temp.db file to copy data
	if(fptr == NULL)
	{
		printf("\nFailed to open file....");
		return;
	}
	while(fread(&i, size, 1, fptr) > 0)						//reading records one by one from inventory.db file
	{
		if(temp == i.id)
		{
			found = 1;
		}
		else
		{
			i.id = ++j;									//for new id in temp file
			fwrite(&i, size, 1, fptr1);					//copies all data except one which we want to delete
		}
	}
	if(found == 0)
	{
		printf("\nItem not found...");
	}
	fclose(fptr);
	fclose(fptr1);
	remove("inventory.db");
	rename("temp.db", "inventory.db");
}

void find()
{
	item_t i;
	FILE *fptr;
	int temp;
	long int size = sizeof(item_t);
	printf("\nEnter ID of item to be searched : ");
	scanf("%d", &temp);
	fptr = fopen("inventory.db", "rb");
	if(fptr == NULL)
	{
		printf("\nFile can not be opened");
		return;
	}
	while(fread(&i, size, 1, fptr) > 0)
	{
		if(temp == i.id)
		{
			printf("\nItem found...");
			printf("\nID : %d", i.id);
			printf("\nName : %s", i.name);
			printf("\nPrice : %d", i.price);
			printf("\nQuantity : %d", i.qty);
			fclose(fptr);
			return;
		}
	}
}


void display()
{
	item_t i;
	FILE *fptr;
	long int size = sizeof(item_t);
	fptr = fopen("inventory.db", "rb");
	if(fptr == NULL)
	{
		printf("\nFile can not be opened");
		return;
	}
	while(fread(&i, size, 1, fptr) > 0)
	{
		printf("\n\nId : %-5d", i.id);
		printf("\nName : %-20s", i.name);
		printf("\nPrice : %-5.2lf", i.price);
		printf("\nQuantity : %-5d", i.qty);
	}
	fclose(fptr);
	return;
}
